                                                    *Basic Docker Example*


Steps to run the application:

        -> Step1: Clone this gitlab reporitory to your local machine

        -> Step2: Make sure Docker engine is running in the background of your system

        -> Step3: Open the cloned folder on your most preferred Source-Code editor (I used Visual Studio Code)

        -> Step4: Now to run the committed image, run a docker command in the terminal as follows
           "docker run --name fibofacto -p 5000:5000 fibonacci-factorial"
        
        -> If the above step is run properly, you can see the logs below this command in the terminal as follows:

        PS E:\dock\Fibonacci-Flask> docker run --name fibofacto -p 5000:5000 fibonacci-factorial
        * Serving Flask app 'main' (lazy loading)
        * Environment: production
          WARNING: This is a development server. Do not use it in a production deployment.Use a production WSGI server instead.
        * Debug mode: on
        * Running on all addresses.
          WARNING: This is a development server. Do not use it in a production deployment.
        * Running on http://172.17.0.2:5000/ (Press CTRL+C to quit)
        * Restarting with stat
        * Debugger is active!
        * Debugger PIN: 163-637-377 

        -> Step5: Go to your browser and type 'localhost:5000' in the url input box.

        You can now see the application up and running :)


How to Access the API:

        -> This step can be found on the browser screen once you can succesfully run the application

        
