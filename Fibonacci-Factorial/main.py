from flask import Flask, render_template, jsonify, request
app = Flask(__name__)


def fibo(params):
  i = 0
  j = 1
  sequence = []
  current_operation = 0
  for current_n in range(0, params+1):
    # We appends now, since f(0) = 0 = i_initial , f(1) = 1 =j_initial
    sequence.append(i)
    # prepare next opération
    current_operation = i + j
    i = j
    j = current_operation

  return sequence


def factorial(n):
    if n == 0:
        return 1
    else:
        return n * factorial(n-1)



@app.route("/")
def hello_world():
    return render_template('index.html')

@app.route("/fibonacci/<int:param>/")
def get_fibo(param):
    series = fibo(param)
    return f"Fibonacci Series of {param} is {series}"

@app.route("/factorial/<int:param>/")
def get_fact(param):
    series = factorial(param)
    return f"Factorial of {param} is {series}"

    
if __name__=="__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)